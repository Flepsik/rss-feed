package flepsik.github.com.rssreader.screens.subscribtions.impl.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import flepsik.github.com.rssreader.R;
import flepsik.github.com.rssreader.model.Subscription;

public final class AddSubscriptionDialogFragment extends DialogFragment {
    public static final String TAG = "AddSubscriptionDialogFragment";

    private OnAddSubscriptionDialogListener listener;
    private EditText urlView;
    private EditText nameView;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_add_subscription, null);
        urlView = (EditText) view.findViewById(R.id.edit_url);
        nameView = (EditText) view.findViewById(R.id.edit_name);

        builder.setView(view)
                .setPositiveButton(R.string.add, (null))
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                    closeDialog();
                });
        return builder.create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnAddSubscriptionDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnAddSubscriptionDialogListener");
        }
    }

    // Prevent dialog from closing on Add clicked
    @Override
    public void onResume() {
        super.onResume();
        final AlertDialog dialog = (AlertDialog) getDialog();
        if (dialog != null) {
            Button positiveButton = dialog.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(v -> onPositiveButtonClicked());
        }
    }

    private void onPositiveButtonClicked() {
        String url = urlView.getText().toString();
        if (Patterns.WEB_URL.matcher(url).matches() && listener != null) {
            listener.onSubscriptionAdded(new Subscription(url, nameView.getText().toString()));
            closeDialog();
        } else {
            Toast.makeText(getContext(), "Incorrect url, please try again", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private void closeDialog() {
        getDialog().cancel();
    }

    interface OnAddSubscriptionDialogListener {
        void onSubscriptionAdded(Subscription subscription);
    }
}