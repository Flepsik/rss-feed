package flepsik.github.com.rssreader;

import javax.inject.Singleton;

import dagger.Component;
import flepsik.github.com.rssreader.screens.rss_feed.inject.RssFeedComponent;
import flepsik.github.com.rssreader.screens.rss_feed.inject.RssFeedModule;
import flepsik.github.com.rssreader.screens.subscribtions.inject.SubscriptionsComponent;
import flepsik.github.com.rssreader.screens.subscribtions.inject.SubscriptionsModule;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    SubscriptionsComponent plus(SubscriptionsModule module);

    RssFeedComponent plus(RssFeedModule module);
}
