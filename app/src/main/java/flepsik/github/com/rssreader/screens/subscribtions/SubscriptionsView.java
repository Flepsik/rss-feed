package flepsik.github.com.rssreader.screens.subscribtions;

import java.util.List;

import flepsik.github.com.rssreader.common.mvp.BindView;
import flepsik.github.com.rssreader.model.Subscription;

public interface SubscriptionsView extends BindView {
    void showSubscriptions(List<Subscription> subscriptions);

    void showSubscription(Subscription subscription, int index);

    void removeSubscription(int index);

    void openSubscription(Subscription subscription);

    void selectItem(int index, boolean selected);

    void showProgress(boolean shouldShow);

    void showEmptyPlaceholder(boolean shouldShow);

    void setSelectedMode(int count, boolean selectedMode);
}
