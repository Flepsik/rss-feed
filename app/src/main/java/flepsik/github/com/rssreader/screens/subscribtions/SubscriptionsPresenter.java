package flepsik.github.com.rssreader.screens.subscribtions;

import flepsik.github.com.rssreader.common.mvp.Presenter;
import flepsik.github.com.rssreader.model.Subscription;

public interface SubscriptionsPresenter extends Presenter<SubscriptionsView> {
    void addSubscription(Subscription subscription);

    void removeSubscription(int index);

    void onItemClicked(int index);

    void onLongClicked(int index);

    void onDeleteClicked();
}
