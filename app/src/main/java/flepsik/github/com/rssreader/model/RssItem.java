package flepsik.github.com.rssreader.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class RssItem implements Parcelable {
    private long id = -1;
    private String url;
    private String title;
    private String description;
    private long subscriptionId;

    public RssItem() {
    }

    public RssItem(String url, String title, String description) {
        this.url = url;
        this.title = title;
        this.description = description;
    }

    public RssItem(long id, String url, String title, String description) {
        this.id = id;
        this.url = url;
        this.title = title;
        this.description = description;
    }

    public RssItem(long id, String url, String title, String description, long subscriptionId) {
        this.id = id;
        this.url = url;
        this.title = title;
        this.description = description;
        this.subscriptionId = subscriptionId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(long subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.url);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeLong(this.subscriptionId);
    }

    protected RssItem(Parcel in) {
        this.id = in.readLong();
        this.url = in.readString();
        this.title = in.readString();
        this.description = in.readString();
        this.subscriptionId = in.readLong();
    }

    public static final Parcelable.Creator<RssItem> CREATOR = new Parcelable.Creator<RssItem>() {
        @Override
        public RssItem createFromParcel(Parcel source) {
            return new RssItem(source);
        }

        @Override
        public RssItem[] newArray(int size) {
            return new RssItem[size];
        }
    };
}
