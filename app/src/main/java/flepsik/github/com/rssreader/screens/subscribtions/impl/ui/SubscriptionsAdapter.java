package flepsik.github.com.rssreader.screens.subscribtions.impl.ui;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import flepsik.github.com.rssreader.R;
import flepsik.github.com.rssreader.model.Subscription;

class SubscriptionsAdapter extends RecyclerView.Adapter<SubscriptionsAdapter.ViewHolder> {
    private List<Subscription> items = new ArrayList<>();

    public SubscriptionsAdapter() {
    }

    public void setItems(List<Subscription> itemList) {
        items = new ArrayList<>(itemList);
        notifyDataSetChanged();
    }

    public void addItem(Subscription subscription, int index) {
        items.add(index, subscription);
        notifyItemInserted(index);
    }

    public void removeItem(int index) {
        items.remove(index);
        notifyItemRemoved(index);
    }

    public void selectItem(int index, boolean state) {
        items.get(index).setSelected(state);
        notifyItemChanged(index);
    }

    @Override
    public SubscriptionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.view_subscription_item, parent, false
        );
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SubscriptionsAdapter.ViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView url;
        View rootView;

        ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            url = (TextView) itemView.findViewById(R.id.url);
            rootView = itemView.findViewById(R.id.root_view);
        }

        void bind(Subscription subscription) {
            url.setText(subscription.getUrl());
            title.setText(subscription.getTitle());
            title.setVisibility(TextUtils.isEmpty(title.getText()) ? View.GONE : View.VISIBLE);
            rootView.setSelected(subscription.isSelected());

        }
    }
}