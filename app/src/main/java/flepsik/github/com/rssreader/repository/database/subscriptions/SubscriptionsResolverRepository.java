package flepsik.github.com.rssreader.repository.database.subscriptions;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.Message;

import java.util.ArrayList;
import java.util.List;

import flepsik.github.com.rssreader.common.LoadingCallback;
import flepsik.github.com.rssreader.model.Subscription;
import flepsik.github.com.rssreader.repository.database.ContractClass;
import flepsik.github.com.rssreader.repository.database.DatabaseUtils;
import flepsik.github.com.rssreader.repository.database.RssDatabaseHelper;

public class SubscriptionsResolverRepository implements SubscriptionsRepository {
    private ContentResolver resolver;

    public SubscriptionsResolverRepository(ContentResolver contentResolver) {
        resolver = contentResolver;
    }

    @Override
    public void insert(Subscription subscription) {
        AsyncQueryHandler handler = new AsyncQueryHandler(resolver) {
            @Override
            protected void onInsertComplete(int token, Object cookie, Uri uri) {
                super.onInsertComplete(token, cookie, uri);
                if (uri != null) {
                    long id = ContentUris.parseId(uri);
                    if (id > 0) {
                        subscription.setId(id);
                    }
                }
            }
        };

        handler.startInsert(0, null,
                ContractClass.Subscription.CONTENT_URI, DatabaseUtils.toContentValues(subscription)
        );
    }

    @Override
    public void update(Subscription subscription) {
        AsyncQueryHandler handler = new AsyncQueryHandler(resolver) {
        };

        handler.startUpdate(0, null,
                ContentUris.withAppendedId(ContractClass.Subscription.CONTENT_ID_URI_BASE, subscription.getId()),
                DatabaseUtils.toContentValues(subscription),
                null, null
        );
    }

    @Override
    public void delete(long index) {
        AsyncQueryHandler handler = new AsyncQueryHandler(resolver) {
        };
        handler.startDelete(0, null,
                ContentUris.withAppendedId(ContractClass.Subscription.CONTENT_ID_URI_BASE, index),
                null, null);
    }

    @Override
    public void findAll(LoadingCallback<List<Subscription>> callback) {
        AsyncQueryHandler handler = new AsyncQueryHandler(resolver) {
            @Override
            protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
                super.onQueryComplete(token, cookie, cursor);
                List<Subscription> result = new ArrayList<>();
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        do {
                            int id = cursor.getInt(cursor.getColumnIndex(RssDatabaseHelper.KEY_SUB_ID));
                            String title = cursor.getString(cursor.getColumnIndex(RssDatabaseHelper.KEY_SUB_TITLE));
                            String url = cursor.getString(cursor.getColumnIndex(RssDatabaseHelper.KEY_SUB_URL));
                            result.add(new Subscription(id, url, title));
                        } while (cursor.moveToNext());
                    }
                    cursor.close();
                }
                callback.onSuccess(result);
            }
        };
        handler.startQuery(0, null,
                ContractClass.Subscription.CONTENT_URI,
                ContractClass.Subscription.DEFAULT_PROJECTION,
                null, null, null);
    }

    @Override
    public void find(int index, LoadingCallback<Subscription> callback) {
        AsyncQueryHandler handler = new AsyncQueryHandler(resolver) {
            @Override
            protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
                super.onQueryComplete(token, cookie, cursor);
                Subscription subscription = null;
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        long id = cursor.getLong(cursor.getColumnIndex(RssDatabaseHelper.KEY_SUB_ID));
                        String title = cursor.getString(cursor.getColumnIndex(RssDatabaseHelper.KEY_SUB_TITLE));
                        String url = cursor.getString(cursor.getColumnIndex(RssDatabaseHelper.KEY_SUB_URL));
                        subscription = new Subscription(id, url, title);
                    }
                    cursor.close();
                }
                callback.onSuccess(subscription);
            }
        };
        handler.startQuery(index, null,
                ContentUris.withAppendedId(ContractClass.Subscription.CONTENT_ID_URI_BASE, index),
                ContractClass.Subscription.DEFAULT_PROJECTION,
                null, null, null);
    }
}
