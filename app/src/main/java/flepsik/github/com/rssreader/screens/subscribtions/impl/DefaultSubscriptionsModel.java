package flepsik.github.com.rssreader.screens.subscribtions.impl;

import java.util.List;

import flepsik.github.com.rssreader.common.LoadingCallback;
import flepsik.github.com.rssreader.model.Subscription;
import flepsik.github.com.rssreader.repository.database.subscriptions.SubscriptionsRepository;
import flepsik.github.com.rssreader.screens.subscribtions.SubscriptionsModel;

public class DefaultSubscriptionsModel implements SubscriptionsModel {
    private SubscriptionsRepository repository;

    public DefaultSubscriptionsModel(SubscriptionsRepository repository) {
        this.repository = repository;
    }

    @Override
    public void addItem(Subscription subscription) {
        repository.insert(subscription);
    }

    @Override
    public void removeItem(Subscription subscription) {
        repository.delete(subscription.getId());
    }

    @Override
    public void getAllItems(LoadingCallback<List<Subscription>> callback) {
        repository.findAll(callback);
    }
}
