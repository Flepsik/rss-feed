package flepsik.github.com.rssreader.repository.database.subscriptions;

import flepsik.github.com.rssreader.model.Subscription;
import flepsik.github.com.rssreader.repository.database.Repository;

public interface SubscriptionsRepository extends Repository<Subscription> {
}
