package flepsik.github.com.rssreader.repository.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.HashMap;

public class RssProvider extends ContentProvider {
    private static HashMap<String, String> sSubscriptionsProjectionMap;
    private static HashMap<String, String> sRssItemsProjectionMap;
    private static final int SUBSCRIPTIONS = 1;
    private static final int SUBSCRIPTIONS_ID = 2;
    private static final int RSS_ITEMS = 3;
    private static final int RSS_ITEMS_ID = 4;
    private static final UriMatcher sUriMatcher;

    private RssDatabaseHelper databaseHelper;

    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(ContractClass.AUTHORITY, "subscriptions", SUBSCRIPTIONS);
        sUriMatcher.addURI(ContractClass.AUTHORITY, "subscriptions/#", SUBSCRIPTIONS_ID);
        sUriMatcher.addURI(ContractClass.AUTHORITY, "rss_items", RSS_ITEMS);
        sUriMatcher.addURI(ContractClass.AUTHORITY, "rss_items/#", RSS_ITEMS_ID);

        sSubscriptionsProjectionMap = new HashMap<>();
        for (int i = 0; i < ContractClass.Subscription.DEFAULT_PROJECTION.length; i++) {
            sSubscriptionsProjectionMap.put(
                    ContractClass.Subscription.DEFAULT_PROJECTION[i],
                    ContractClass.Subscription.DEFAULT_PROJECTION[i]);
        }
        sRssItemsProjectionMap = new HashMap<>();
        for (int i = 0; i < ContractClass.RssItem.DEFAULT_PROJECTION.length; i++) {
            sRssItemsProjectionMap.put(
                    ContractClass.RssItem.DEFAULT_PROJECTION[i],
                    ContractClass.RssItem.DEFAULT_PROJECTION[i]);
        }
    }


    @Override
    public boolean onCreate() {
        databaseHelper = new RssDatabaseHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String orderBy = null;
        String id;
        switch (sUriMatcher.match(uri)) {
            case SUBSCRIPTIONS:
                qb.setTables(ContractClass.Subscription.TABLE_NAME);
                qb.setProjectionMap(sSubscriptionsProjectionMap);
                if (TextUtils.isEmpty(sortOrder)) {
                    orderBy = ContractClass.Subscription.DEFAULT_SORT_ORDER;
                }
                break;
            case SUBSCRIPTIONS_ID:
                id = uri.getPathSegments()
                        .get(ContractClass.Subscription.SUBSCRIPTIONS_ID_PATH_POSITION);
                qb.setTables(ContractClass.Subscription.TABLE_NAME);
                qb.setProjectionMap(sSubscriptionsProjectionMap);
                qb.appendWhere(ContractClass.Subscription.COLUMN_NAME_ID + "=" + id);
                if (TextUtils.isEmpty(sortOrder)) {
                    orderBy = ContractClass.Subscription.DEFAULT_SORT_ORDER;
                }
                break;
            case RSS_ITEMS:
                qb.setTables(ContractClass.RssItem.TABLE_NAME);
                qb.setProjectionMap(sRssItemsProjectionMap);
                if (TextUtils.isEmpty(sortOrder)) {
                    orderBy = ContractClass.RssItem.DEFAULT_SORT_ORDER;
                }
                break;
            case RSS_ITEMS_ID:
                id = uri.getPathSegments()
                        .get(ContractClass.RssItem.RSS_ID_PATH_POSITION);
                qb.setTables(ContractClass.RssItem.TABLE_NAME);
                qb.setProjectionMap(sRssItemsProjectionMap);
                qb.appendWhere(ContractClass.RssItem.COLUMN_NAME_ID + "=" + id);
                if (TextUtils.isEmpty(sortOrder)) {
                    orderBy = ContractClass.RssItem.DEFAULT_SORT_ORDER;
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        Cursor cursor = qb.query(db, projection, selection, selectionArgs, null, null, orderBy);
        if (getContext() != null) {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case SUBSCRIPTIONS:
                return ContractClass.Subscription.CONTENT_TYPE;
            case SUBSCRIPTIONS_ID:
                return ContractClass.Subscription.CONTENT_ITEM_TYPE;
            case RSS_ITEMS:
                return ContractClass.RssItem.CONTENT_TYPE;
            case RSS_ITEMS_ID:
                return ContractClass.RssItem.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues initialValues) {
        if (sUriMatcher.match(uri) != SUBSCRIPTIONS &&
                sUriMatcher.match(uri) != RSS_ITEMS) {
            throw new IllegalArgumentException("Unknown URI " + uri);
        }
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        ContentValues values;
        if (initialValues != null) {
            values = new ContentValues(initialValues);
        } else {
            values = new ContentValues();
        }
        long rowId = -1;
        Uri rowUri = Uri.EMPTY;
        switch (sUriMatcher.match(uri)) {
            case SUBSCRIPTIONS:
//                DatabaseUtils.normalizeSubscriptionValues(values);
                rowId = db.insertWithOnConflict(ContractClass.Subscription.TABLE_NAME,
                        null,
                        values,
                        SQLiteDatabase.CONFLICT_REPLACE);
                if (rowId > 0 && getContext() != null) {
                    rowUri = ContentUris.withAppendedId(ContractClass.Subscription.CONTENT_ID_URI_BASE, rowId);
                    getContext().getContentResolver().notifyChange(rowUri, null);
                }
                break;
            case RSS_ITEMS:
                rowId = db.insertWithOnConflict(ContractClass.RssItem.TABLE_NAME,
                        null,
                        values,
                        SQLiteDatabase.CONFLICT_REPLACE);
                if (rowId > 0 && getContext() != null) {
                    rowUri = ContentUris.withAppendedId(ContractClass.RssItem.CONTENT_ID_URI_BASE, rowId);
                    getContext().getContentResolver().notifyChange(rowUri, null);
                }
                break;
        }
        return rowUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String where, @Nullable String[] whereArgs) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        int count;
        String specificWhere;
        String id;
        switch (sUriMatcher.match(uri)) {
            case SUBSCRIPTIONS:
                count = db.delete(ContractClass.Subscription.TABLE_NAME, where, whereArgs);
                break;
            case SUBSCRIPTIONS_ID:
                id = uri.getPathSegments()
                        .get(ContractClass.Subscription.SUBSCRIPTIONS_ID_PATH_POSITION);
                specificWhere = ContractClass.Subscription.COLUMN_NAME_ID + " = " + id;
                if (where != null) {
                    specificWhere = specificWhere + " AND " + where;
                }
                count = db.delete(ContractClass.Subscription.TABLE_NAME, specificWhere, whereArgs);
                break;
            case RSS_ITEMS:
                count = db.delete(ContractClass.RssItem.TABLE_NAME, where, whereArgs);
                break;
            case RSS_ITEMS_ID:
                id = uri.getPathSegments().get(ContractClass.RssItem.RSS_ID_PATH_POSITION);
                specificWhere = ContractClass.RssItem.COLUMN_NAME_ID + " = " + id;
                if (where != null) {
                    specificWhere = specificWhere + " AND " + where;
                }
                count = db.delete(ContractClass.RssItem.TABLE_NAME, specificWhere, whereArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (getContext() != null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String where, @Nullable String[] whereArgs) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        int count;
        String specificWhere;
        String id;
        switch (sUriMatcher.match(uri)) {
            case SUBSCRIPTIONS:
                count = db.update(ContractClass.Subscription.TABLE_NAME, contentValues, where, whereArgs);
                break;
            case SUBSCRIPTIONS_ID:
                id = uri.getPathSegments()
                        .get(ContractClass.Subscription.SUBSCRIPTIONS_ID_PATH_POSITION);
                specificWhere = ContractClass.Subscription.COLUMN_NAME_ID + " = " + id;
                if (where != null) {
                    specificWhere = specificWhere + " AND " + where;
                }
                count = db.update(ContractClass.Subscription.TABLE_NAME, contentValues, specificWhere, whereArgs);
                break;
            case RSS_ITEMS:
                count = db.update(ContractClass.RssItem.TABLE_NAME, contentValues, where, whereArgs);
                break;
            case RSS_ITEMS_ID:
                id = uri.getPathSegments().get(ContractClass.RssItem.RSS_ID_PATH_POSITION);
                specificWhere = ContractClass.RssItem.COLUMN_NAME_ID + " = " + id;
                if (where != null) {
                    specificWhere = specificWhere + " AND " + where;
                }
                count = db.update(ContractClass.RssItem.TABLE_NAME, contentValues, specificWhere, whereArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (getContext() != null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }
}
