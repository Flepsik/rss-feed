package flepsik.github.com.rssreader.screens.rss_feed.impl;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import flepsik.github.com.rssreader.R;
import flepsik.github.com.rssreader.model.RssItem;
import flepsik.github.com.rssreader.model.Subscription;

class RssFeedAdapter extends RecyclerView.Adapter<RssFeedAdapter.ViewHolder> {
    private List<RssItem> items = new ArrayList<>();

    public RssFeedAdapter() {
    }

    public void setItems(List<RssItem> itemList) {
        items = new ArrayList<>(itemList);
        notifyDataSetChanged();
    }

    public void addItem(RssItem rssItem, int index) {
        items.add(index, rssItem);
        notifyItemInserted(index);
    }

    @Override
    public RssFeedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.view_rss_item, parent, false
        );
        return new RssFeedAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RssFeedAdapter.ViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView description;

        ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            description = (TextView) itemView.findViewById(R.id.description);
        }

        void bind(RssItem rssItem) {
            title.setText(rssItem.getTitle());
            description.setText(rssItem.getDescription());
        }
    }
}
