package flepsik.github.com.rssreader.screens.subscribtions;

import java.util.List;

import flepsik.github.com.rssreader.common.LoadingCallback;
import flepsik.github.com.rssreader.model.Subscription;

public interface SubscriptionsModel {
    void addItem(Subscription subscription);

    void removeItem(Subscription subscription);

    void getAllItems(LoadingCallback<List<Subscription>> callback);
}
