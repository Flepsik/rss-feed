package flepsik.github.com.rssreader.common;

public interface LoadingCallback<Item> {
    void onSuccess(Item item);

    void onError(Exception e);
}
