package flepsik.github.com.rssreader.repository.database;

import android.net.Uri;

public class ContractClass {
    public static final String AUTHORITY = "flepsik.github.com.rssreader.repository.database.ContractClass";

    public static final class Subscription {
        public static final String TABLE_NAME = "subscriptions";
        private static final String SCHEME = "content://";
        private static final String PATH_SUBSCRIPTIONS = "/subscriptions";
        private static final String PATH_SUBSCRIPTIONS_ID = "/subscriptions/";
        public static final Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_SUBSCRIPTIONS);
        public static final Uri CONTENT_ID_URI_BASE = Uri.parse(SCHEME + AUTHORITY + PATH_SUBSCRIPTIONS_ID);

        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.github.flepsik." + TABLE_NAME;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.com.github.flepsik." + TABLE_NAME;

        public static final String COLUMN_NAME_ID = "_id";
        public static final String COLUMN_NAME_TITLE = "name";
        public static final String COLUMN_NAME_URL = "url";
        public static final String[] DEFAULT_PROJECTION = new String[] {
                Subscription.COLUMN_NAME_ID,
                Subscription.COLUMN_NAME_TITLE,
                Subscription.COLUMN_NAME_URL,
        };

        public static final int SUBSCRIPTIONS_ID_PATH_POSITION = 1;
        public static final String DEFAULT_SORT_ORDER = COLUMN_NAME_ID + " DESC";

        private Subscription() {
        }
    }

    public static final class RssItem {
        public static final String TABLE_NAME = "rss_items";
        private static final String SCHEME = "content://";
        private static final String PATH_RSS_ITEMS = "/rss_items";
        private static final String PATH_RSS_ITEMS_ID = "/rss_items/";
        public static final Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_RSS_ITEMS);
        public static final Uri CONTENT_ID_URI_BASE = Uri.parse(SCHEME + AUTHORITY + PATH_RSS_ITEMS_ID);

        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.github.flepsik." + TABLE_NAME;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.com.github.flepsik." + TABLE_NAME;

        public static final String COLUMN_NAME_ID = "_id";
        public static final String COLUMN_NAME_TITLE = "name";
        public static final String COLUMN_NAME_DESCRIPTION = "description";
        public static final String COLUMN_NAME_URL = "url";
        public static final String COLUMN_NAME_SUBSCRIPTION_ID = "subscription_id";

        public static final String[] DEFAULT_PROJECTION = new String[] {
                RssItem.COLUMN_NAME_ID,
                RssItem.COLUMN_NAME_TITLE,
                RssItem.COLUMN_NAME_DESCRIPTION,
                RssItem.COLUMN_NAME_URL,
                RssItem.COLUMN_NAME_SUBSCRIPTION_ID
        };

        public static final int RSS_ID_PATH_POSITION = 1;
        public static final String DEFAULT_SORT_ORDER = COLUMN_NAME_ID + " DESC";

        private RssItem() {
        }
    }

    private ContractClass() {
    }
}
