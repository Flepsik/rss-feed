package flepsik.github.com.rssreader.repository.database;

import java.util.List;

import flepsik.github.com.rssreader.common.LoadingCallback;

public interface Repository<Item> {
    void insert(Item item);

    void update(Item item);

    void delete(long id);

    void findAll(LoadingCallback<List<Item>> callback);

    void find(int index, LoadingCallback<Item> callback);
}
