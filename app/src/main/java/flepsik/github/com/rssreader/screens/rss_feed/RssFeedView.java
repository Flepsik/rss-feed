package flepsik.github.com.rssreader.screens.rss_feed;

import java.util.List;

import flepsik.github.com.rssreader.common.mvp.BindView;
import flepsik.github.com.rssreader.model.RssItem;

public interface RssFeedView extends BindView {
    void showItems(List<RssItem> items);

    void showItem(int index, RssItem item);

    void showEmptyPlaceholder(boolean shouldShow);

    void showProgress(boolean shouldShow);

    void showRefreshing(boolean shouldShow);

    void openRssItem(RssItem item);

    void setTitle(String title);
}
