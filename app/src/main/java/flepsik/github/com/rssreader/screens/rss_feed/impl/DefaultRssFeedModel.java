package flepsik.github.com.rssreader.screens.rss_feed.impl;

import java.util.List;

import flepsik.github.com.rssreader.common.LoadingCallback;
import flepsik.github.com.rssreader.model.RssItem;
import flepsik.github.com.rssreader.model.Subscription;
import flepsik.github.com.rssreader.repository.network.NetworkRepository;
import flepsik.github.com.rssreader.repository.database.rss_feed.RssFeedRepository;
import flepsik.github.com.rssreader.screens.rss_feed.RssFeedModel;

public class DefaultRssFeedModel implements RssFeedModel {
    private final RssFeedRepository databaseRepository;
    private final NetworkRepository networkRepository;

    public DefaultRssFeedModel(final RssFeedRepository repository, NetworkRepository networkRepository) {
        this.databaseRepository = repository;
        this.networkRepository = networkRepository;
    }

    @Override
    public void loadItems(Subscription subscription, LoadingCallback<List<RssItem>> callback) {
        networkRepository.loadRssItems(subscription.getId(), subscription.getUrl(), new LoadingCallback<List<RssItem>>() {
            @Override
            public void onSuccess(List<RssItem> items) {
                items.forEach(databaseRepository::insert);
                callback.onSuccess(items);
            }

            @Override
            public void onError(Exception e) {
                databaseRepository.findAll(subscription.getId(), callback);
            }
        });
    }
}
