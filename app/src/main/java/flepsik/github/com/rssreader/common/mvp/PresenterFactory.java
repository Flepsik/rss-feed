package flepsik.github.com.rssreader.common.mvp;

public interface PresenterFactory<View extends BindView, P extends Presenter<View>> {
    P create();
}
