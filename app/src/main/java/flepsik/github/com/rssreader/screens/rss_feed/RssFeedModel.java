package flepsik.github.com.rssreader.screens.rss_feed;

import java.util.List;

import flepsik.github.com.rssreader.common.LoadingCallback;
import flepsik.github.com.rssreader.model.RssItem;
import flepsik.github.com.rssreader.model.Subscription;

public interface RssFeedModel {
    void loadItems(Subscription subscription, LoadingCallback<List<RssItem>> callback);
}
