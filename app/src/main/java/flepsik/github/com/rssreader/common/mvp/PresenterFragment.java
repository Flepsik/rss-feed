package flepsik.github.com.rssreader.common.mvp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

import javax.inject.Inject;

import flepsik.github.com.rssreader.common.utils.PresenterUtils;

public abstract class PresenterFragment<P extends Presenter<View>, View extends BindView> extends Fragment {
    @Inject
    protected P presenter;
    protected String tag;
    private PresenterDelegator<View> presenterDelegator;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tag = loadTag(savedInstanceState);
        inject();
        presenterDelegator = new PresenterDelegator<>(presenter);
        presenterDelegator.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(android.view.View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenterDelegator.bindView(getBindView());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenterDelegator.unbindView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenterDelegator.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenterDelegator.saveState(outState);
        saveTag(outState);
    }

    protected String loadTag(Bundle savedInstance) {
        String tag = PresenterUtils.loadTag(savedInstance);
        if (TextUtils.isEmpty(tag)) {
            tag = generateTag();
        }

        return tag;
    }

    protected void saveTag(Bundle outState) {
        PresenterUtils.saveTag(outState, tag);
    }

    protected String generateTag() {
        return PresenterUtils.generateUniqueTag();
    }

    public abstract void inject();

    @NonNull
    public abstract View getBindView();
}
