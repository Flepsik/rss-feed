package flepsik.github.com.rssreader.repository.database;

import android.content.ContentValues;
import android.text.TextUtils;

import org.w3c.dom.Text;

import flepsik.github.com.rssreader.model.RssItem;
import flepsik.github.com.rssreader.model.Subscription;

public class DatabaseUtils {
    public static ContentValues toContentValues(Subscription subscription) {
        ContentValues result = new ContentValues();
        if (!TextUtils.isEmpty(subscription.getTitle())) {
            result.put(RssDatabaseHelper.KEY_SUB_TITLE, subscription.getTitle());
        }

        if (!TextUtils.isEmpty(subscription.getUrl())) {
            result.put(RssDatabaseHelper.KEY_SUB_URL, subscription.getUrl());
        }

        if (subscription.getId() != -1) {
            result.put(RssDatabaseHelper.KEY_SUB_ID, subscription.getId());
        }
        return result;
    }

    public static ContentValues toContentValues(RssItem rssItem) {
        ContentValues result = new ContentValues();
        if (!TextUtils.isEmpty(rssItem.getTitle())) {
            result.put(RssDatabaseHelper.KEY_RSS_ITEM_TITLE, rssItem.getTitle());
        }

        if (!TextUtils.isEmpty(rssItem.getDescription())) {
            result.put(RssDatabaseHelper.KEY_RSS_ITEM_DESCRIPTION, rssItem.getDescription());
        }

        if (!TextUtils.isEmpty(rssItem.getUrl())) {
            result.put(RssDatabaseHelper.KEY_RSS_ITEM_URL, rssItem.getUrl());
        }

        if (rssItem.getId() != -1) {
            result.put(RssDatabaseHelper.KEY_RSS_ITEM_ID, rssItem.getId());
        }

        if (rssItem.getSubscriptionId() != -1) {
            result.put(RssDatabaseHelper.KEY_RSS_ITEM_SUBSCRIPTION_ID, rssItem.getSubscriptionId());
        }
        return result;
    }

    public static void normalizeSubscriptionValues(ContentValues contentValues) {
        if (!contentValues.containsKey(ContractClass.Subscription.COLUMN_NAME_TITLE)) {
            contentValues.put(ContractClass.Subscription.COLUMN_NAME_TITLE, "");
        }
    }

    public static void normalizeRssItemValues(ContentValues contentValues) {

    }
}
