package flepsik.github.com.rssreader.screens.rss_feed.impl;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import java.util.List;

import flepsik.github.com.rssreader.App;
import flepsik.github.com.rssreader.R;
import flepsik.github.com.rssreader.common.RecyclerItemClickListener;
import flepsik.github.com.rssreader.common.mvp.PresenterActivity;
import flepsik.github.com.rssreader.common.utils.NetworkUtils;
import flepsik.github.com.rssreader.model.RssItem;
import flepsik.github.com.rssreader.model.Subscription;
import flepsik.github.com.rssreader.screens.rss_feed.RssFeedPresenter;
import flepsik.github.com.rssreader.screens.rss_feed.RssFeedView;
import flepsik.github.com.rssreader.screens.rss_feed.inject.RssFeedModule;
import flepsik.github.com.rssreader.screens.rss_item.RssItemActivity;

public class RssFeedActivity extends PresenterActivity<RssFeedView, RssFeedPresenter> implements RssFeedView {
    public static final String KEY_SUBSCRIPTION = "Subscription";

    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private RssFeedAdapter adapter;
    private ProgressBar progressBar;
    private View emptyPlaceholder;

    public static void start(Context context, Subscription subscription) {
        Intent intent = new Intent(context, RssFeedActivity.class);
        intent.putExtra(KEY_SUBSCRIPTION, subscription);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rss_feed);

        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        refreshLayout.setOnRefreshListener(() -> presenter.onRefreshCalled());
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        initializeRecycler();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        boolean shouldShowArrow = !isTaskRoot();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(shouldShowArrow);
        }

        progressBar = (ProgressBar) findViewById(R.id.progress);
        emptyPlaceholder = findViewById(R.id.empty_placeholder);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void inject() {
        App.getComponent()
                .plus(new RssFeedModule(tag, getIntent().getParcelableExtra(KEY_SUBSCRIPTION)))
                .inject(this);
    }

    @NonNull
    @Override
    public RssFeedView getBindView() {
        return this;
    }

    @Override
    public void showItems(List<RssItem> items) {
        adapter.setItems(items);
    }

    @Override
    public void showItem(int index, RssItem item) {
        adapter.addItem(item, index);
    }

    @Override
    public void showEmptyPlaceholder(boolean shouldShow) {
        emptyPlaceholder.setVisibility(shouldShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showProgress(boolean shouldShow) {
        progressBar.setVisibility(shouldShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showRefreshing(boolean shouldShow) {
        refreshLayout.setRefreshing(shouldShow);
    }

    @Override
    public void openRssItem(RssItem item) {
        ActionBar actionBar = getSupportActionBar();
        String title = "";
        if (actionBar != null && actionBar.getTitle() != null) {
            title = actionBar.getTitle().toString();
        }
        RssItemActivity.start(this, item, title);
    }

    @Override
    public void setTitle(String title) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    private void initializeRecycler() {
        adapter = new RssFeedAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        presenter.onItemClicked(position);
                    }
                }));
    }
}
