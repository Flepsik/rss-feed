package flepsik.github.com.rssreader.repository.network;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import flepsik.github.com.rssreader.common.LoadingCallback;
import flepsik.github.com.rssreader.common.utils.RssUtils;
import flepsik.github.com.rssreader.model.RssItem;

public class DefaultNetworkRepository implements NetworkRepository {


    @Override
    public void loadRssItems(long subscriptionId, String url, LoadingCallback<List<RssItem>> callback) {
        new FetchFeedTask(url, subscriptionId, callback).execute();
    }

    private class FetchFeedTask extends AsyncTask<Void, Void, List<RssItem>> {

        private String urlLink;
        private long subscriptionId;
        private LoadingCallback<List<RssItem>> callback;

        public FetchFeedTask(String urlLink, long subscriptionId, LoadingCallback<List<RssItem>> callback) {
            this.urlLink = urlLink;
            this.subscriptionId = subscriptionId;
            this.callback = callback;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected List<RssItem> doInBackground(Void... voids) {
            List<RssItem> result = null;
            if (!TextUtils.isEmpty(urlLink)) {
                try {
                    if (!urlLink.startsWith("http://") && !urlLink.startsWith("https://"))
                        urlLink = "http://" + urlLink;

                    URL url = new URL(urlLink);
                    InputStream inputStream = url.openConnection().getInputStream();
                    result = RssUtils.parseFeed(inputStream);
                    for (RssItem items : result) {
                        items.setSubscriptionId(subscriptionId);
                    }
                } catch (IOException | XmlPullParserException e) {
                    e.printStackTrace();
                }
            }
            return result;
        }

        @Override
        protected void onPostExecute(List<RssItem> success) {
            if (success != null) {
                callback.onSuccess(success);
            } else {
                callback.onError(new Exception());
            }
        }
    }
}
