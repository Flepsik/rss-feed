package flepsik.github.com.rssreader.screens.subscribtions.inject;

import dagger.Module;
import dagger.Provides;
import flepsik.github.com.rssreader.common.annotations.ActivityScope;
import flepsik.github.com.rssreader.common.mvp.PresenterCache;
import flepsik.github.com.rssreader.repository.database.subscriptions.SubscriptionsRepository;
import flepsik.github.com.rssreader.screens.subscribtions.SubscriptionsModel;
import flepsik.github.com.rssreader.screens.subscribtions.impl.DefaultSubscriptionsModel;
import flepsik.github.com.rssreader.screens.subscribtions.SubscriptionsPresenter;
import flepsik.github.com.rssreader.screens.subscribtions.impl.DefaultSubscriptionPresenter;

@Module
public class SubscriptionsModule {
    private String tag;

    public SubscriptionsModule(String tag) {
        this.tag = tag;
    }

    @ActivityScope
    @Provides
    SubscriptionsPresenter providePresenter(SubscriptionsModel model) {
        return PresenterCache.getInstance().getPresenter(tag,
                () -> new DefaultSubscriptionPresenter(model),
                PresenterCache.CacheType.TEMPORARY);
    }

    @ActivityScope
    @Provides
    SubscriptionsModel provideModel(SubscriptionsRepository repository) {
        return new DefaultSubscriptionsModel(repository);
    }
}
