package flepsik.github.com.rssreader.screens.rss_feed.impl;

import android.content.Context;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import flepsik.github.com.rssreader.common.LoadingCallback;
import flepsik.github.com.rssreader.common.mvp.BasePresenter;
import flepsik.github.com.rssreader.common.mvp.Presenter;
import flepsik.github.com.rssreader.common.utils.NetworkUtils;
import flepsik.github.com.rssreader.model.RssItem;
import flepsik.github.com.rssreader.model.Subscription;
import flepsik.github.com.rssreader.screens.rss_feed.RssFeedModel;
import flepsik.github.com.rssreader.screens.rss_feed.RssFeedPresenter;
import flepsik.github.com.rssreader.screens.rss_feed.RssFeedView;

import static flepsik.github.com.rssreader.common.utils.NetworkUtils.TYPE_NOT_CONNECTED;

public class DefaultRssFeedPresenter extends BasePresenter<RssFeedView> implements RssFeedPresenter {

    private final RssFeedModel model;
    private final Subscription subscription;
    private List<RssItem> items = new ArrayList<>();
    @LoadingState
    private int state = STATUS_NONE;

    public DefaultRssFeedPresenter(final RssFeedModel model, final Subscription subscription, Context context) {
        this.model = model;
        this.subscription = subscription;
        initialize();
    }

    @Override
    public void updateView() {
        super.updateView();
        RssFeedView view = view();
        if (view != null) {
            view.showProgress(state == STATUS_LOADING);
            view.showRefreshing(state == STATUS_REFRESHING);
            view.showEmptyPlaceholder(state == STATUS_LOADED_ALL && items.size() == 0);
            view.showItems(items);
            view.setTitle(
                    TextUtils.isEmpty(subscription.getTitle()) ?
                            subscription.getUrl() : subscription.getTitle()
            );
        }
    }

    @Override
    public void onItemClicked(int index) {
        RssFeedView view = view();
        if (view != null) {
            view.openRssItem(items.get(index));
        }
    }

    @Override
    public void onRefreshCalled() {
        RssFeedView view = view();
        if (state != STATUS_LOADING) {
            state = STATUS_REFRESHING;
            model.loadItems(subscription, new RssLoadingCallback());
        }
        if (view != null) {
            view.showRefreshing(state == STATUS_REFRESHING);
        }
    }

    private void initialize() {
        state = STATUS_LOADING;
        model.loadItems(subscription, new RssLoadingCallback());
    }

    private class RssLoadingCallback implements LoadingCallback<List<RssItem>> {

        @Override
        public void onSuccess(List<RssItem> items) {
            state = STATUS_LOADED_ALL;
            DefaultRssFeedPresenter.this.items = items;
            updateView();
        }

        @Override
        public void onError(Exception e) {

        }
    }
}
