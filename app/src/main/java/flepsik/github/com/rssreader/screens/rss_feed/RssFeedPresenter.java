package flepsik.github.com.rssreader.screens.rss_feed;

import flepsik.github.com.rssreader.common.mvp.Presenter;

public interface RssFeedPresenter extends Presenter<RssFeedView> {
    void onItemClicked(int index);

    void onRefreshCalled();
}
