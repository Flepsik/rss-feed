package flepsik.github.com.rssreader.repository.database.rss_feed;

import java.util.List;

import flepsik.github.com.rssreader.common.LoadingCallback;
import flepsik.github.com.rssreader.model.RssItem;
import flepsik.github.com.rssreader.repository.database.Repository;

public interface RssFeedRepository extends Repository<RssItem> {
    void findAll(long subscriptionId, LoadingCallback<List<RssItem>> callback);
}
