package flepsik.github.com.rssreader.screens.subscribtions.impl;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import flepsik.github.com.rssreader.common.LoadingCallback;
import flepsik.github.com.rssreader.common.mvp.BasePresenter;
import flepsik.github.com.rssreader.model.Subscription;
import flepsik.github.com.rssreader.screens.subscribtions.SubscriptionsModel;
import flepsik.github.com.rssreader.screens.subscribtions.SubscriptionsPresenter;
import flepsik.github.com.rssreader.screens.subscribtions.SubscriptionsView;

public class DefaultSubscriptionPresenter extends BasePresenter<SubscriptionsView> implements SubscriptionsPresenter {
    private final SubscriptionsModel model;
    private List<Subscription> subscriptions = new ArrayList<>();
    private boolean isSelectingMode = false;

    @LoadingState
    private int state = STATUS_NONE;

    public DefaultSubscriptionPresenter(final SubscriptionsModel model) {
        this.model = model;
        initialize();
    }

    @Override
    public void updateView() {
        super.updateView();
        SubscriptionsView view = view();
        if (view != null) {
            view.showSubscriptions(subscriptions);
            updateLoadingState(view);
            updatePlaceholderState(view);
            updateSelectingMode();
        }
    }

    @Override
    public void addSubscription(Subscription subscription) {
        subscriptions.add(0, subscription);
        model.addItem(subscription);
        SubscriptionsView view = view();
        if (view != null) {
            view.showSubscription(subscription, 0);
            updatePlaceholderState(view);
        }
    }

    @Override
    public void removeSubscription(int index) {
        Subscription subscription = subscriptions.get(index);
        subscriptions.remove(index);
        model.removeItem(subscription);
        SubscriptionsView view = view();
        if (view != null) {
            view.removeSubscription(index);
            updatePlaceholderState(view);
        }
    }

    @Override
    public void onItemClicked(int index) {
        SubscriptionsView view = view();
        if (view != null) {
            if (isSelectingMode) {
                Subscription subscription = subscriptions.get(index);
                subscription.setSelected(!subscription.isSelected());
                view.selectItem(index, subscription.isSelected());
            } else {
                view.openSubscription(subscriptions.get(index));
            }
            updateSelectingMode();
        }
    }

    @Override
    public void onLongClicked(int index) {
        if (!isSelectingMode) {
            isSelectingMode = true;
            subscriptions.get(index).setSelected(true);
            SubscriptionsView view = view();
            if (view != null) {
                view.selectItem(index, true);
            }
            updateSelectingMode();
        }
    }

    @Override
    public void onDeleteClicked() {
        if (isSelectingMode) {
            SubscriptionsView view = view();
            for (Iterator<Subscription> itemIterator = subscriptions.iterator(); itemIterator.hasNext(); ) {
                Subscription subscription = itemIterator.next();
                if (subscription.isSelected()) {
                    int index = subscriptions.indexOf(subscription);
                    model.removeItem(subscription);
                    itemIterator.remove();
                    if (view != null) {
                        view.removeSubscription(index);
                    }
                }
            }
            updateSelectingMode();
            if (view != null) {
                updatePlaceholderState(view);
            }
        }
    }

    private void updateSelectingMode() {
        int selectedItems = 0;
        for (Subscription subscription : subscriptions) {
            if (subscription.isSelected()) {
                selectedItems++;
            }
        }
        isSelectingMode = selectedItems > 0;
        SubscriptionsView view = view();
        if (view != null) {
            view.setSelectedMode(selectedItems, isSelectingMode);
        }
    }

    private void updateLoadingState(@NonNull SubscriptionsView view) {
        view.showProgress(state == STATUS_LOADING);
    }

    private void updatePlaceholderState(@NonNull SubscriptionsView view) {
        view.showEmptyPlaceholder(subscriptions.size() == 0 && state == STATUS_LOADED_ALL);
    }

    private void initialize() {
        state = STATUS_LOADING;
        model.getAllItems(new LoadingCallback<List<Subscription>>() {
            @Override
            public void onSuccess(List<Subscription> subscription) {
                state = STATUS_LOADED_ALL;
                subscriptions = subscription;
                updateView();
            }

            @Override
            public void onError(Exception e) {
                state = STATUS_READY;
                updateView();
            }
        });
    }
}
