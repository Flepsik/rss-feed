package flepsik.github.com.rssreader.screens.subscribtions.inject;

import dagger.Subcomponent;
import flepsik.github.com.rssreader.common.annotations.ActivityScope;
import flepsik.github.com.rssreader.screens.subscribtions.impl.ui.SubscriptionsActivity;

@ActivityScope
@Subcomponent(modules = SubscriptionsModule.class)
public interface SubscriptionsComponent {
    SubscriptionsActivity inject(SubscriptionsActivity activity);
}
