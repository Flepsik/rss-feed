package flepsik.github.com.rssreader.screens.rss_feed.inject;

import dagger.Subcomponent;
import flepsik.github.com.rssreader.common.annotations.ActivityScope;
import flepsik.github.com.rssreader.screens.rss_feed.impl.RssFeedActivity;
import flepsik.github.com.rssreader.screens.subscribtions.impl.ui.SubscriptionsActivity;
import flepsik.github.com.rssreader.screens.subscribtions.inject.SubscriptionsModule;

@ActivityScope
@Subcomponent(modules = RssFeedModule.class)
public interface RssFeedComponent {
    RssFeedActivity inject(RssFeedActivity activity);
}
