package flepsik.github.com.rssreader;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import flepsik.github.com.rssreader.repository.network.DefaultNetworkRepository;
import flepsik.github.com.rssreader.repository.network.NetworkRepository;
import flepsik.github.com.rssreader.repository.database.rss_feed.RssFeedRepository;
import flepsik.github.com.rssreader.repository.database.rss_feed.RssFeedResolverRepository;
import flepsik.github.com.rssreader.repository.database.subscriptions.SubscriptionsRepository;
import flepsik.github.com.rssreader.repository.database.subscriptions.SubscriptionsResolverRepository;

@Module
public class AppModule {
    private Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public Application provideApplication() {
        return application;
    }

    @Provides
    @Singleton
    public SubscriptionsRepository provideSubscriptionsRepository(Application application) {
        return new SubscriptionsResolverRepository(application.getContentResolver());
    }

    @Provides
    @Singleton
    public RssFeedRepository provideRssFeedRepository(Application application) {
        return new RssFeedResolverRepository(application.getContentResolver());
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    public NetworkRepository provideNetworkRepository() {
        return new DefaultNetworkRepository();
    }
}
