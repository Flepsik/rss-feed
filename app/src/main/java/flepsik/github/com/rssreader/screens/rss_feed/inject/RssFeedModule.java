package flepsik.github.com.rssreader.screens.rss_feed.inject;


import android.content.Context;

import dagger.Module;
import dagger.Provides;
import flepsik.github.com.rssreader.common.annotations.ActivityScope;
import flepsik.github.com.rssreader.common.mvp.PresenterCache;
import flepsik.github.com.rssreader.model.Subscription;
import flepsik.github.com.rssreader.repository.database.rss_feed.RssFeedRepository;
import flepsik.github.com.rssreader.repository.network.NetworkRepository;
import flepsik.github.com.rssreader.screens.rss_feed.RssFeedModel;
import flepsik.github.com.rssreader.screens.rss_feed.RssFeedPresenter;
import flepsik.github.com.rssreader.screens.rss_feed.impl.DefaultRssFeedModel;
import flepsik.github.com.rssreader.screens.rss_feed.impl.DefaultRssFeedPresenter;

@Module
public class RssFeedModule {
    private String tag;
    private Subscription subscription;

    public RssFeedModule(String tag, Subscription subscription) {
        this.tag = tag;
        this.subscription = subscription;
    }

    @ActivityScope
    @Provides
    RssFeedPresenter providePresenter(RssFeedModel model, Context context) {
        return PresenterCache.getInstance().getPresenter(tag,
                () -> new DefaultRssFeedPresenter(model, subscription, context),
                PresenterCache.CacheType.TEMPORARY);
    }

    @ActivityScope
    @Provides
    RssFeedModel provideModel(RssFeedRepository repository, NetworkRepository networkRepository) {
        return new DefaultRssFeedModel(repository, networkRepository);
    }
}
