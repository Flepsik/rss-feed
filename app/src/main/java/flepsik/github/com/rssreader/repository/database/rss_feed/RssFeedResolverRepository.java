package flepsik.github.com.rssreader.repository.database.rss_feed;

import android.app.Application;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

import flepsik.github.com.rssreader.common.LoadingCallback;
import flepsik.github.com.rssreader.model.RssItem;
import flepsik.github.com.rssreader.model.Subscription;
import flepsik.github.com.rssreader.repository.database.ContractClass;
import flepsik.github.com.rssreader.repository.database.DatabaseUtils;
import flepsik.github.com.rssreader.repository.database.RssDatabaseHelper;

public class RssFeedResolverRepository implements RssFeedRepository {
    private ContentResolver resolver;

    public RssFeedResolverRepository(ContentResolver contentResolver) {
        resolver = contentResolver;
    }


    @Override
    public void insert(RssItem rssItem) {
        AsyncQueryHandler handler = new AsyncQueryHandler(resolver) {
            @Override
            protected void onInsertComplete(int token, Object cookie, Uri uri) {
                super.onInsertComplete(token, cookie, uri);
                if (uri != null) {
                    long id = ContentUris.parseId(uri);
                    if (id > 0) {
                        rssItem.setId(id);
                    }
                }
            }
        };

        handler.startInsert(0, null,
                ContractClass.RssItem.CONTENT_URI, DatabaseUtils.toContentValues(rssItem)
        );
    }

    @Override
    public void update(RssItem rssItem) {
        AsyncQueryHandler handler = new AsyncQueryHandler(resolver) {
        };

        handler.startUpdate(0, null,
                ContentUris.withAppendedId(ContractClass.RssItem.CONTENT_ID_URI_BASE, rssItem.getId()),
                DatabaseUtils.toContentValues(rssItem),
                null, null
        );
    }

    @Override
    public void delete(long index) {
        AsyncQueryHandler handler = new AsyncQueryHandler(resolver) {
        };
        handler.startDelete(0, null,
                ContentUris.withAppendedId(ContractClass.RssItem.CONTENT_ID_URI_BASE, index),
                null, null);
    }

    @Override
    public void findAll(LoadingCallback<List<RssItem>> callback) {
        AsyncQueryHandler handler = new AsyncQueryHandler(resolver) {
            @Override
            protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
                super.onQueryComplete(token, cookie, cursor);
                List<RssItem> result = new ArrayList<>();
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        do {
                            long id = cursor.getLong(cursor.getColumnIndex(RssDatabaseHelper.KEY_RSS_ITEM_ID));
                            String title = cursor.getString(cursor.getColumnIndex(RssDatabaseHelper.KEY_RSS_ITEM_TITLE));
                            String description = cursor.getString(cursor.getColumnIndex(RssDatabaseHelper.KEY_RSS_ITEM_DESCRIPTION));
                            String url = cursor.getString(cursor.getColumnIndex(RssDatabaseHelper.KEY_RSS_ITEM_URL));
                            long subscriptionId = cursor.getLong(cursor.getColumnIndex(RssDatabaseHelper.KEY_RSS_ITEM_SUBSCRIPTION_ID));
                            result.add(new RssItem(id, url, title, description, subscriptionId));
                        } while (cursor.moveToNext());
                    }
                    cursor.close();
                }
                callback.onSuccess(result);
            }
        };
        handler.startQuery(0, null,
                ContractClass.RssItem.CONTENT_URI,
                ContractClass.RssItem.DEFAULT_PROJECTION,
                null, null, null);
    }

    @Override
    public void find(int index, LoadingCallback<RssItem> callback) {
        AsyncQueryHandler handler = new AsyncQueryHandler(resolver) {
            @Override
            protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
                super.onQueryComplete(token, cookie, cursor);
                RssItem rssItem = null;
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        long id = cursor.getLong(cursor.getColumnIndex(RssDatabaseHelper.KEY_RSS_ITEM_ID));
                        String title = cursor.getString(cursor.getColumnIndex(RssDatabaseHelper.KEY_RSS_ITEM_TITLE));
                        String description = cursor.getString(cursor.getColumnIndex(RssDatabaseHelper.KEY_RSS_ITEM_DESCRIPTION));
                        String url = cursor.getString(cursor.getColumnIndex(RssDatabaseHelper.KEY_RSS_ITEM_URL));
                        long subscriptionId = cursor.getLong(cursor.getColumnIndex(RssDatabaseHelper.KEY_RSS_ITEM_SUBSCRIPTION_ID));
                        rssItem = new RssItem(id, url, title, description, subscriptionId);
                    }
                    cursor.close();
                }
                callback.onSuccess(rssItem);
            }
        };
        handler.startQuery(index, null,
                ContentUris.withAppendedId(ContractClass.RssItem.CONTENT_ID_URI_BASE, index),
                ContractClass.RssItem.DEFAULT_PROJECTION,
                null, null, null);
    }

    @Override
    public void findAll(long subscriptionId, LoadingCallback<List<RssItem>> callback) {
        AsyncQueryHandler handler = new AsyncQueryHandler(resolver) {
            @Override
            protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
                super.onQueryComplete(token, cookie, cursor);
                List<RssItem> result = new ArrayList<>();
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        do {
                            long id = cursor.getLong(cursor.getColumnIndex(RssDatabaseHelper.KEY_RSS_ITEM_ID));
                            String title = cursor.getString(cursor.getColumnIndex(RssDatabaseHelper.KEY_RSS_ITEM_TITLE));
                            String description = cursor.getString(cursor.getColumnIndex(RssDatabaseHelper.KEY_RSS_ITEM_DESCRIPTION));
                            String url = cursor.getString(cursor.getColumnIndex(RssDatabaseHelper.KEY_RSS_ITEM_URL));
                            long subscriptionId = cursor.getLong(cursor.getColumnIndex(RssDatabaseHelper.KEY_RSS_ITEM_SUBSCRIPTION_ID));
                            result.add(new RssItem(id, url, title, description, subscriptionId));
                        } while (cursor.moveToNext());
                    }
                    cursor.close();
                }
                callback.onSuccess(result);
            }
        };
        String where = ContractClass.RssItem.COLUMN_NAME_SUBSCRIPTION_ID + " = " + subscriptionId;
        handler.startQuery(0, null,
                ContractClass.RssItem.CONTENT_URI,
                ContractClass.RssItem.DEFAULT_PROJECTION,
                where, null, null);
    }
}
