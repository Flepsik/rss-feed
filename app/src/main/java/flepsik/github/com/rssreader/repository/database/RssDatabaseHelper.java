package flepsik.github.com.rssreader.repository.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import flepsik.github.com.rssreader.model.Subscription;

public class RssDatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "RssDatabase";
    private static final int DATABASE_VERSION = 3;

    public static final String DATABASE_TABLE_SUBSCRIPTIONS = ContractClass.Subscription.TABLE_NAME;
    public static final String KEY_SUB_ID = ContractClass.Subscription.COLUMN_NAME_ID;
    public static final String KEY_SUB_TITLE = ContractClass.Subscription.COLUMN_NAME_TITLE;
    public static final String KEY_SUB_URL = ContractClass.Subscription.COLUMN_NAME_URL;
    private static final String DATABASE_CREATE_TABLE_SUBSCRIPTIONS =
            "create table " + DATABASE_TABLE_SUBSCRIPTIONS + "("
                    + KEY_SUB_ID + " integer primary key autoincrement, "
                    + KEY_SUB_TITLE + " string, "
                    + KEY_SUB_URL + " string);";

    public static final String DATABASE_TABLE_RSS_ITEMS = ContractClass.RssItem.TABLE_NAME;
    public static final String KEY_RSS_ITEM_ID = ContractClass.RssItem.COLUMN_NAME_ID;
    public static final String KEY_RSS_ITEM_TITLE = ContractClass.RssItem.COLUMN_NAME_TITLE;
    public static final String KEY_RSS_ITEM_DESCRIPTION = ContractClass.RssItem.COLUMN_NAME_DESCRIPTION;
    public static final String KEY_RSS_ITEM_URL = ContractClass.RssItem.COLUMN_NAME_URL;
    public static final String KEY_RSS_ITEM_SUBSCRIPTION_ID = ContractClass.RssItem.COLUMN_NAME_SUBSCRIPTION_ID;
    private static final String DATABASE_CREATE_TABLE_RSS_ITEM =
            "create table " + DATABASE_TABLE_RSS_ITEMS + " ("
                    + KEY_RSS_ITEM_ID + " integer primary key autoincrement, "
                    + KEY_RSS_ITEM_TITLE + " string, "
                    + KEY_RSS_ITEM_DESCRIPTION + " string, "
                    + KEY_RSS_ITEM_URL + " string not null, "
                    + KEY_RSS_ITEM_SUBSCRIPTION_ID + " integer not null, "
                    + " foreign key (" + KEY_RSS_ITEM_SUBSCRIPTION_ID +
                    ") references " + DATABASE_TABLE_SUBSCRIPTIONS + "(" + KEY_SUB_ID + ") on delete cascade);";

    RssDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DATABASE_CREATE_TABLE_SUBSCRIPTIONS);
        sqLiteDatabase.execSQL(DATABASE_CREATE_TABLE_RSS_ITEM);

        sqLiteDatabase.insert(DATABASE_TABLE_SUBSCRIPTIONS, null,
                DatabaseUtils.toContentValues(
                        new Subscription("https://lenta.ru/rss/news", "Lenta.ru"))
        );

        sqLiteDatabase.insert(DATABASE_TABLE_SUBSCRIPTIONS, null,
                DatabaseUtils.toContentValues(
                        new Subscription("http://rss.nytimes.com/services/xml/rss/nyt/Europe.xml"
                                , "The New York Times"))
        );

        sqLiteDatabase.insert(DATABASE_TABLE_SUBSCRIPTIONS, null,
                DatabaseUtils.toContentValues(
                        new Subscription("https://www.sports.ru/rss/main.xml", "Sports.ru"))
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_SUBSCRIPTIONS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_RSS_ITEMS);
        onCreate(sqLiteDatabase);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        db.execSQL("PRAGMA foreign_keys=ON");
    }
}
