package flepsik.github.com.rssreader.screens.rss_item;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

import flepsik.github.com.rssreader.R;
import flepsik.github.com.rssreader.common.mvp.PresenterActivity;
import flepsik.github.com.rssreader.model.RssItem;

public class RssItemActivity extends AppCompatActivity {
    public static final String KEY_RSS_ITEM = "rss_item";
    public static final String KEY_RSS_TITLE = "title";

    public static void start(Context context, RssItem item, String title) {
        Intent intent = new Intent(context, RssItemActivity.class);
        intent.putExtra(KEY_RSS_ITEM, item);
        intent.putExtra(KEY_RSS_TITLE, title);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rss_item);
        RssItem item = getIntent().getParcelableExtra(KEY_RSS_ITEM);
        String title = getIntent().getStringExtra(KEY_RSS_TITLE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(TextUtils.isEmpty(title) ? item.getTitle() : title);
        setSupportActionBar(toolbar);
        boolean shouldShowArrow = !isTaskRoot();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(shouldShowArrow);
        }

        TextView titleView = (TextView) findViewById(R.id.title);
        TextView descriptionView = (TextView) findViewById(R.id.description);
        TextView urlView = (TextView) findViewById(R.id.url);
        findViewById(R.id.root_view).setOnClickListener(view -> {
            Uri address = Uri.parse(item.getUrl());
            Intent openLinkIntent = new Intent(Intent.ACTION_VIEW, address);
            startActivity(openLinkIntent);
        });

        titleView.setText(item.getTitle());
        descriptionView.setText(item.getDescription());
        urlView.setText(item.getUrl());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
