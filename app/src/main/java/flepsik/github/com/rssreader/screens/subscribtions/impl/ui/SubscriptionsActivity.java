package flepsik.github.com.rssreader.screens.subscribtions.impl.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import flepsik.github.com.rssreader.App;
import flepsik.github.com.rssreader.R;
import flepsik.github.com.rssreader.common.RecyclerItemClickListener;
import flepsik.github.com.rssreader.common.mvp.PresenterActivity;
import flepsik.github.com.rssreader.model.Subscription;
import flepsik.github.com.rssreader.screens.rss_feed.impl.RssFeedActivity;
import flepsik.github.com.rssreader.screens.subscribtions.SubscriptionsPresenter;
import flepsik.github.com.rssreader.screens.subscribtions.SubscriptionsView;
import flepsik.github.com.rssreader.screens.subscribtions.inject.SubscriptionsModule;


public class SubscriptionsActivity
        extends PresenterActivity<SubscriptionsView, SubscriptionsPresenter>
        implements SubscriptionsView, AddSubscriptionDialogFragment.OnAddSubscriptionDialogListener {

    private RecyclerView recyclerView;
    private SubscriptionsAdapter adapter;
    private ProgressBar progressBar;
    private MenuItem deleteButton;
    private View emptyPlaceholder;
    private boolean isSelectingMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscriptions);

        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        initializeRecycler();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        boolean shouldShowArrow = !isTaskRoot();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(shouldShowArrow);
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(view -> showAddDialog());

        progressBar = (ProgressBar) findViewById(R.id.progress);
        emptyPlaceholder = findViewById(R.id.empty_placeholder);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        deleteButton = menu.findItem(R.id.action_delete);
        deleteButton.setVisible(isSelectingMode);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == deleteButton.getItemId()) {
            presenter.onDeleteClicked();
            return true;
        } else if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void inject() {
        App.getComponent()
                .plus(new SubscriptionsModule(tag))
                .inject(this);
    }

    @NonNull
    @Override
    public SubscriptionsView getBindView() {
        return this;
    }

    @Override
    public void onSubscriptionAdded(Subscription subscription) {
        Toast.makeText(this, "new RSS feed " + subscription.toString(), Toast.LENGTH_SHORT).show();
        presenter.addSubscription(subscription);
    }

    @Override
    public void showSubscriptions(List<Subscription> subscriptions) {
        adapter.setItems(subscriptions);
    }

    @Override
    public void showSubscription(Subscription subscription, int index) {
        adapter.addItem(subscription, index);
    }

    @Override
    public void removeSubscription(int index) {
        adapter.removeItem(index);
    }

    @Override
    public void openSubscription(Subscription subscription) {
        RssFeedActivity.start(this, subscription);
    }

    @Override
    public void selectItem(int index, boolean selected) {
        adapter.selectItem(index, selected);
    }

    @Override
    public void showProgress(boolean shouldShow) {
        progressBar.setVisibility(shouldShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showEmptyPlaceholder(boolean shouldShow) {
        emptyPlaceholder.setVisibility(shouldShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setSelectedMode(int selectedCount, boolean selectedMode) {
        isSelectingMode = selectedMode;
        ActionBar toolbar = getSupportActionBar();
        if (toolbar != null) {
            if (selectedMode) {
                toolbar.setTitle(getString(R.string.selected, String.valueOf(selectedCount)));
            } else {
                toolbar.setTitle(getTitle());
            }
        }
        if (deleteButton != null) {
            deleteButton.setVisible(isSelectingMode);
        }
    }

    private void showAddDialog() {
        DialogFragment dialog = new AddSubscriptionDialogFragment();
        dialog.show(getSupportFragmentManager(), AddSubscriptionDialogFragment.TAG);
    }

    private void initializeRecycler() {
        adapter = new SubscriptionsAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        presenter.onItemClicked(position);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        presenter.onLongClicked(position);
                    }
                }));
    }
}
