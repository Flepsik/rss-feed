package flepsik.github.com.rssreader.repository.network;

import java.util.List;

import flepsik.github.com.rssreader.common.LoadingCallback;
import flepsik.github.com.rssreader.model.RssItem;
import flepsik.github.com.rssreader.model.Subscription;

public interface NetworkRepository {
    void loadRssItems(long subscriptionId, String url, LoadingCallback<List<RssItem>> callback);
}
